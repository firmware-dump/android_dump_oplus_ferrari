{
    "Image_Build_IDs": {
        "adsp": "ADSP.HT.5.7-00999.4-WAIPIO-1.13871.8", 
        "aop": "AOP.HO.4.0-00429-WAIPIO_E-1", 
        "apps_LE": "LE.UM.5.3.1.r1-11900-genericarmv8-64.0-1", 
        "boot": "BOOT.MXF.2.0-00864.1-WAIPIO-1", 
        "btfm": "BTFW.HSP.2.1.0-00435-PATCHZ-1", 
        "cdsp": "CDSP.HT.2.7-00851.1-WAIPIO-1", 
        "common": "Waipio.LA.2.0.r1-00123-STD.PROD-1", 
        "cpucp": "CPUCP.FW.1.0-00090-WAIPIO.EXT-2", 
        "cpuss_vm": "CPUSS.CPUSYS.VM.1.0-00019-WAIPIO.EXT-1", 
        "glue": "GLUE.WAIPIO_LA.2.0-00004-NOOP_TEST_WAIPIO-1", 
        "modem": "MPSS.DE.2.0-00780.7-WAIPIO_GEN_PACK-1.10279.9", 
        "slpi": "SLPI.HY.5.0-00192-WAIPIO-1", 
        "spss": "SPSS.A1.1.6-07677-WAIPIO-2", 
        "tz": "TZ.XF.5.18-00252-SPF.WAIPIO-1", 
        "tz_apps": "TZ.APPS.1.18-00285-SPF.WAIPIO-1", 
        "wlan_hsp": "WLAN.HSP.2.0.c10-00017-QCAHSPSWPL_V1_V2_SILICONZ-1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "Waipio.LA.2.0.r1-00123-STD.PROD-1", 
        "Product_Flavor": "asic", 
        "Time_Stamp": "2023-03-14 01:05:45"
    }, 
    "Version": "1.0"
}